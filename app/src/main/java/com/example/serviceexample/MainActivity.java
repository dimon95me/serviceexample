package com.example.serviceexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "my_tag";

    private Button action_start, action_stop;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        action_start = findViewById(R.id.main_action_start_service);
        action_stop = findViewById(R.id.main_action_stop_service);

        intent = new Intent(this, MyService.class);

        action_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(getApplicationContext(), MyService.class));
                Log.d(TAG, "onStartClickService: asdf");
            }
        });

        action_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(view.getContext(), MyService.class));
            }
        });

    }




    public void onStartClickService(){
        startService(intent);
        Log.d(TAG, "onStartClickService: asdf");
    }

    public void onStopClickService(){
        stopService(intent);
    }


}
